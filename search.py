def contains(array, element_to_search):
    
    index = 0

    found = False

    while (index < len(array) and found == False):

        if array[index] == element_to_search :
            found = True

        index += 1

    return found

def contains_sorted(array, element_to_search):

    index = 0

    found = False

    while (index < len(array) and array[index] >= element_to_search and found == False):

        if array[index] == element_to_search:
            found = True

        index += 1

    return found


def suit_term(first_term, raison, n):
    index = 0
    while index < n:
        if index == 0:
            U = first_term
        else:
            U = U + raison
        index += 1
    return U


def suit_term_recursive(first_term, raison, n):
    if n <= 1:
        return first_term
    else:
        return suit_term_recursive(first_term, raison, n-1) + raison

if __name__ == '__main__':
    array = [1, 2, 3, 4]
    print(contains(array, 1))
    print(contains(array, 5))

    print(contains_sorted(array, 1))
    print(contains_sorted(array, 5))
    
