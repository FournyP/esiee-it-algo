package main

import (
	"fmt"
	"math"
)

const hz = 0.0000001

func PrintForXSecondes(second int) {
	fmt.Println("Pour", second, "secondes : ")

	instruction := float64(second) / (hz)

	fmt.Println("Nombre d'instruction possible : ")
	fmt.Println(instruction)

	fmt.Println("log(n) :", math.Log10(instruction))
	fmt.Println("n :", instruction)
	fmt.Println("n log(n) :", instruction*math.Log10(instruction))
	fmt.Println("n² :", math.Sqrt(instruction))
	fmt.Println("2^n :", math.Pow(2, instruction))
	fmt.Println()
}

func main() {
	PrintForXSecondes(1)
	PrintForXSecondes(1 * 60)
	PrintForXSecondes(1 * 60 * 60)
	PrintForXSecondes(1 * 60 * 60 * 24)
}
