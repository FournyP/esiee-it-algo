package main

import "math"

func NlognCalcul(value int) float64 {
	return float64(value) * math.Log(float64(value))
}

func Nlogn(interval [2]int) {
	MakeChart("n log(n)", "nlogn.png", interval, NlognCalcul)
}
