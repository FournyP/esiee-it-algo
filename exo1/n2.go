package main

import "math"

func N2Calcul(value int) float64 {
	return math.Pow(float64(value), 2)
}

func N2(interval [2]int) {
	MakeChart("n²", "n2.png", interval, N2Calcul)
}
