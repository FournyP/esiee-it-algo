package main

import "fmt"

func PermutationQuadratique(arrayToTest []int) bool {

	index1 := 0
	permutation := true

	for index1 < len(arrayToTest) && permutation == true {
		index2 := 0
		for index2 < len(arrayToTest) && permutation == true {

			if (index1 != index2 && arrayToTest[index1] == arrayToTest[index2]) || (arrayToTest[index2] < 1 || arrayToTest[index2] > len(arrayToTest)) {
				permutation = false
			}

			index2++
		}

		index1++
	}

	return permutation
}

func PermutationLineaire(arrayToTest []int) bool {

	tempArray := make([]int, len(arrayToTest)+1)
	permutation := true

	for index := 0; index < len(arrayToTest); index++ {
		if arrayToTest[index] < 1 || arrayToTest[index] > len(arrayToTest) {
			permutation = false
		}
	}

	if permutation == true {

		for index := 0; index < len(arrayToTest); index++ {
			tempArray[arrayToTest[index]]++
			if tempArray[index] > 1 {
				permutation = false
			}
		}
	}

	return permutation
}

func main() {

	array1 := []int{1, 2, 3, 4}
	array2 := []int{2, 4, 6, 6}

	fmt.Println(PermutationQuadratique(array1))
	fmt.Println(PermutationLineaire(array1))

	fmt.Println(PermutationQuadratique(array2))
	fmt.Println(PermutationLineaire(array2))
}
