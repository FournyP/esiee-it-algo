# Author

Fourny Pierre

# Exo 1

![n](https://i.ibb.co/j6wmrdy/n.png)

![nlogn](https://i.ibb.co/YQw1dwq/nlogn.png)

![logn](https://i.ibb.co/qgt8JDB/logn.png)

![n²](https://i.ibb.co/CbQmvb3/n2.png)

![2n](https://i.ibb.co/K56PxRy/2n.png)

# Exo 2

![complexity](https://i.ibb.co/kmmHFvk/image.png)