# About

Petit readme pour l'exercice sur les arbres

## Arbre 1 

- Taille : 4
- Hauteur : 2
- Parcours largeur : 19, 11, 21, 3
- LCI : 1
- LCE : 3
- Nombre d'arbres : 8
- Parcours préfixe : 19, 11, 3, 21
- Parcours postfixe : 3, 11, 21, 19
- Parcours Infixe : 3, 11, 19, 21

## Arbre 2

- Taille : 5
- Hauteur : 2
- Parcours largeur : 77, 63, 109, 68, 81
- LCI : 2
- LCE : 4
- Nombre d'arbres : 11
- Parcours préfixe : 77, 63, 68, 109, 81
- Parcours postfixe : 68, 63, 81, 109, 77
- Parcours Infixe : 63, 68, 77, 81, 109

## Arbre 3

- Taille : 5
- Hauteur : 3
- Parcours largeur : 107, 133, 167, 151, 183
- LCI : 3
- LCE : 6
- Nombre d'arbres : 11
- Parcours préfixe : 107, 133, 167, 151, 183
- Parcours postfixe : 151, 183, 167, 133, 107
- Parcours Infixe : 107, 133, 151, 167, 183

## Arbre 4

- Taille : 7
- Hauteur : 3
- Parcours largeur : 17, 11, 23, 15, 19, 44, 18
- LCI : 4
- LCE : 7
- Nombre d'arbres : 15
- Parcours préfixe : 17, 11, 15, 23, 19, 18, 44
- Parcours postfixe : 15, 11, 18, 19, 44, 23, 17
- Parcours Infixe : 11, 15, 17, 18, 19, 23, 44

## Arbre 5

- Taille : 6
- Hauteur : 3
- Parcours largeur : 45, 39, 48, 14, 41, 17
- LCI : 3
- LCE : 6
- Nombre d'arbres : 12
- Parcours préfixe : 45, 39, 14, 17, 41, 48
- Parcours postfixe : 17, 14, 41, 39, 48, 45
- Parcours Infixe : 14, 17, 39, 41, 45, 48