package main

func NCalcul(value int) float64 {
	return float64(value)
}

func N(interval [2]int) {
	MakeChart("n", "n.png", interval, NCalcul)
}
