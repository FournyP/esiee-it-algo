package main

import "math"

func LognCalcul(value int) float64 {
	return math.Log(float64(value))
}

func Logn(interval [2]int) {
	MakeChart("log(n)", "logn.png", interval, LognCalcul)
}
