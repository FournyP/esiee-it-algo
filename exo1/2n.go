package main

import "math"

func PowNCalcul(value int) float64 {
	return math.Pow(2, float64(value))
}

func PowN(interval [2]int) {
	MakeChart("2n", "2n.png", interval, PowNCalcul)
}
