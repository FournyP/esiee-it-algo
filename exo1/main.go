package main

import (
	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/plotutil"
	"gonum.org/v1/plot/vg"
)

type fn func(int) float64

func InitPlot(title string, len int) (*plot.Plot, plotter.XYs) {
	p := plot.New()

	p.Title.Text = title
	p.X.Label.Text = "X"
	p.Y.Label.Text = "Y"

	points := make(plotter.XYs, len)

	return p, points
}

func SavePlot(p *plot.Plot, filename string) {
	if err := p.Save(4*vg.Inch, 4*vg.Inch, filename); err != nil {
		panic(err)
	}
}

func MakeChart(title string, filename string, interval [2]int, f fn) {

	p, points := InitPlot(title, interval[1])

	for index := interval[0]; index < interval[1]; index++ {
		points[index].X = float64(index)
		points[index].Y = f(index)
	}

	if err := plotutil.AddLinePoints(p, points); err != nil {
		panic(err)
	}

	SavePlot(p, filename)
}

func main() {
	Logn([2]int{1, 1000})
	N([2]int{0, 1000})
	Nlogn([2]int{1, 1000})
	N2([2]int{0, 1000})
	PowN([2]int{0, 10})
}
